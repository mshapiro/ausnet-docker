# Class: docker
#
#  Manages the docker configuration, package and service
#
# Parameters:
#
#  [enabled]
#   Desc: Determine whether the docker daemon should be installed and running
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'docker': enabled => true, }
#
class docker(
  $enabled = false,
  $package_name = 'docker',
  $service_name = 'docker',
  $docker_volume_group = 'vg_docker')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    $file_ensure    = 'file'
    Package["$package_name"] -> File['/etc/sysconfig/docker']
    Package["$package_name"] -> File['/etc/sysconfig/docker-storage-setup']
    File['/etc/sysconfig/docker'] -> Service["$service_name"]
    Exec['docker-storage-setup'] -> Service["$service_name"]
    File['/etc/sysconfig/docker-storage-setup'] ~> Exec['docker_storage_cleanup']
    Exec['docker_storage_cleanup'] -> Exec['docker-storage-setup']
    Exec['docker_storage_cleanup'] ~> Exec['docker-storage-setup']
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    $file_ensure    = 'absent'
    Service["$service_name"] -> File['/etc/sysconfig/docker']
    Service["$service_name"] -> File['/etc/sysconfig/docker-storage-setup']
    File['/etc/sysconfig/docker'] -> Package["$package_name"]
    File['/etc/sysconfig/docker-storage-setup'] -> Package["$package_name"]
    Package["$package_name"] ~> Exec['docker_post_uninstall']
  }
  # resource defaults
  File {
    owner    => 'root',
    group    => 'root',
    mode     => '0644',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => 'system_u',
  }
  Exec {
    path => '/usr/bin:/bin:/usr/sbin:/sbin',
  }
  # resources
  package { "$package_name":
    ensure  =>  $package_ensure,
  }
  file { '/etc/sysconfig/docker':
    ensure   => $file_ensure,
    source   => 'puppet:///modules/docker/docker',
  }
  file { '/etc/sysconfig/docker-storage-setup':
    ensure   => $file_ensure,
    content  => template('docker/docker-storage-setup.erb'),
  }
  exec { 'docker-storage-setup':
    command     =>  'docker-storage-setup',
    onlyif      =>  "vgs $docker_volume_group",
    unless      =>  "echo $package_ensure | grep -qw absent || lvs $docker_volume_group/docker-pool",
  }
  exec { 'docker_storage_cleanup':
    command     =>  "systemctl stop $service_name && rm -rf /var/lib/docker",
    unless      =>  "echo $package_ensure | grep -qw absent || lvs $docker_volume_group/docker-pool",
  }
  exec { 'docker_post_uninstall':
    command     =>  "rm -rf /var/lib/docker /etc/sysconfig/docker* /etc/docker && lvremove -f $docker_volume_group/docker-pool",
    refreshonly =>  true,
  }
  service { "$service_name":
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
}

